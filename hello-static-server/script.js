function makeRequest() {
    document.getElementById("container").innerHTML = "";

    var request = new window.XMLHttpRequest();
    request.open('GET', 'products.json', true);
    request.onload = function() {
        var data = JSON.parse(request.responseText);

        var filterData = data.filter(function (vacancy) {
            if (vacancy.position.toUpperCase().includes(document.getElementById("filter").value.toUpperCase())){
                return vacancy.position;
            }
        });

        put(filterData);
    }

    request.onerror= function(data) {
        console.log(data);
    }
    request.send();
}

function putMessage(product) {
    var position = document.createTextNode(product.position);
    var company = document.createTextNode(product.company);
    var description = document.createTextNode(product.description);

    var container = document.getElementById('container');
    var div = document.createElement('div');
    var h2Position = document.createElement('h2');
    var h2Company = document.createElement('h2');
    var h2Description = document.createElement('h2');

    h2Position.appendChild(position);
    div.appendChild(h2Position);

    h2Company.appendChild(company);
    div.appendChild(h2Company);

    h2Description.appendChild(description);
    div.appendChild(h2Description);

    container.appendChild(div);
}

function put(products) {
    products.forEach(function (value) {
        putMessage(value);
    })
}